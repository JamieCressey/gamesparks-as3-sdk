
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!

package com.gamesparks.api.responses
{
	
	import com.gamesparks.api.types.*;
	import com.gamesparks.*;
	
	/**
	* A response containing the details of the team that was created
	*/
	public class CreateTeamResponse extends GSResponse
	{
	
		public function CreateTeamResponse(data : Object)
		{
			super(data);
		}
	
	
		/** <summary>
		* A JSON object representing the team
		*/ 
		public function getTeam() : Team{
			if(data.team != null)
			{
				return new Team(data.team);
			}
			return null;
		}
	}

}

