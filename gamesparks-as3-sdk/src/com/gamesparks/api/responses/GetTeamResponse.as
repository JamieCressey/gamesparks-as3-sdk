
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!

package com.gamesparks.api.responses
{
	
	import com.gamesparks.api.types.*;
	import com.gamesparks.*;
	
	/**
	* A response containing the details of the requested teams
	*/
	public class GetTeamResponse extends GSResponse
	{
	
		public function GetTeamResponse(data : Object)
		{
			super(data);
		}
	
	
		/** <summary>
		* A JSON object representing the team
		*/ 
		public function getTeam() : Team{
			if(data.team != null)
			{
				return new Team(data.team);
			}
			return null;
		}
		/** <summary>
		* A JSON array of teams.
		*/ 
		public function getTeams() : Vector.<Team>
		{
			var ret : Vector.<Team> = new Vector.<Team>();

			if(data.teams != null)
			{
			 	var list : Array = data.teams;
			 	for(var item : Object in list)
			 	{
				 	ret.push(new Team(list[item]));
			 	}
			}
			
			return ret;
		}
	}

}

