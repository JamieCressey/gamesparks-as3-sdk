
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!

package com.gamesparks.api.responses
{
	
	import com.gamesparks.api.types.*;
	import com.gamesparks.*;
	
	/**
	* A response to a player joining a team or a request for team data
	*/
	public class JoinTeamResponse extends GSResponse
	{
	
		public function JoinTeamResponse(data : Object)
		{
			super(data);
		}
	
	
		/** <summary>
		* A JSON object representing the team
		*/ 
		public function getTeam() : Team{
			if(data.team != null)
			{
				return new Team(data.team);
			}
			return null;
		}
	}

}

