
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!

package com.gamesparks.api.responses
{
	
	import com.gamesparks.api.types.*;
	import com.gamesparks.*;
	
	/**
	* A response containing aggregation data
	*/
	public class GetRunningTotalsResponse extends GSResponse
	{
	
		public function GetRunningTotalsResponse(data : Object)
		{
			super(data);
		}
	
	
		/** <summary>
		* A list of JSON objects representing the aggregation data
		*/ 
		public function getRunningTotals() : Object{
			if(data.runningTotals != null)
			{
				return data.runningTotals;
			}
			return null;
		}
	}

}

