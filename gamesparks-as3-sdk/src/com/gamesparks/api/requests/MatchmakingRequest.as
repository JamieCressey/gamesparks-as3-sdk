
package com.gamesparks.api.requests
{

	import com.gamesparks.api.*;
	import com.gamesparks.api.responses.*;
	import com.gamesparks.*;
	
	
	/**
	* Register this player for matchmaking, using the given skill and matchShortCode.
	* Players looking for a match using the same matchShortCode will be considered for a match, based on the matchConfig.
	* Each player must match the other for the match to be found.
	*/
	public class MatchmakingRequest extends GSRequest
	{
	
		public function setScriptData(scriptData:Object):MatchmakingRequest{
			data["scriptData"] = scriptData;
			return this;
		}
		
		function MatchmakingRequest(gs:GS)
		{
			super(gs);
			data["@class"] =  ".MatchmakingRequest";
		}
		
		/**
		* set the timeout for this request
		*/
		public function setTimeoutSeconds(timeoutSeconds:int=10):MatchmakingRequest
		{
			this.timeoutSeconds = timeoutSeconds; 
			return this;
		}
		
		/**
		* Send the request to the server. The callback function will be invoked with the response
		*/
		public override function send (callback : Function) : void{
			super.send( 
				function(message:Object) : void{
					if(callback != null)
					{
						callback(new MatchmakingResponse(message));
					}
				}
			);
		}
		



		/**
		* The action to take on the already in-flight request for this match. Currently supported actions are: 'cancel'
		*/
		public function setAction( action : String ) : MatchmakingRequest
		{
			this.data["action"] = action;
			return this;
		}


		/**
		* Optional. Players will be grouped based on the distinct value passed in here, only players in the same group can be matched together
		*/
		public function setMatchGroup( matchGroup : String ) : MatchmakingRequest
		{
			this.data["matchGroup"] = matchGroup;
			return this;
		}


		/**
		* The shortCode of the match type this player is registering for
		*/
		public function setMatchShortCode( matchShortCode : String ) : MatchmakingRequest
		{
			this.data["matchShortCode"] = matchShortCode;
			return this;
		}



		/**
		* The skill of the player looking for a match
		*/
		public function setSkill( skill : Number ) : MatchmakingRequest
		{
			this.data["skill"] = skill;
			return this;
		}
				
	}
	
}

