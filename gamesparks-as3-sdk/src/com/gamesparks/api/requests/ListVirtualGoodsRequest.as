
package com.gamesparks.api.requests
{

	import com.gamesparks.api.*;
	import com.gamesparks.api.responses.*;
	import com.gamesparks.*;
	
	
	/**
	* Returns the list of configured virtual goods.
	*/
	public class ListVirtualGoodsRequest extends GSRequest
	{
	
		public function setScriptData(scriptData:Object):ListVirtualGoodsRequest{
			data["scriptData"] = scriptData;
			return this;
		}
		
		function ListVirtualGoodsRequest(gs:GS)
		{
			super(gs);
			data["@class"] =  ".ListVirtualGoodsRequest";
		}
		
		/**
		* set the timeout for this request
		*/
		public function setTimeoutSeconds(timeoutSeconds:int=10):ListVirtualGoodsRequest
		{
			this.timeoutSeconds = timeoutSeconds; 
			return this;
		}
		
		/**
		* Send the request to the server. The callback function will be invoked with the response
		*/
		public override function send (callback : Function) : void{
			super.send( 
				function(message:Object) : void{
					if(callback != null)
					{
						callback(new ListVirtualGoodsResponse(message));
					}
				}
			);
		}
		




		/**
		* A filter to only include goods with the given tags. Each good must have all the provided tags.
		*/
		public function setTags( tags : Vector.<String> ) : ListVirtualGoodsRequest
		{
			this.data["tags"] = toArray(tags);
			return this;
		}
				
	}
	
}

