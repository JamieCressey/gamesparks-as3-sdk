
package com.gamesparks.api.requests
{

	import com.gamesparks.api.*;
	import com.gamesparks.api.responses.*;
	import com.gamesparks.*;
	
	
	/**
	* Get the aggregation data for a group of the player's friends given running total specified by short code.
	*/
	public class GetRunningTotalsRequest extends GSRequest
	{
	
		public function setScriptData(scriptData:Object):GetRunningTotalsRequest{
			data["scriptData"] = scriptData;
			return this;
		}
		
		function GetRunningTotalsRequest(gs:GS)
		{
			super(gs);
			data["@class"] =  ".GetRunningTotalsRequest";
		}
		
		/**
		* set the timeout for this request
		*/
		public function setTimeoutSeconds(timeoutSeconds:int=10):GetRunningTotalsRequest
		{
			this.timeoutSeconds = timeoutSeconds; 
			return this;
		}
		
		/**
		* Send the request to the server. The callback function will be invoked with the response
		*/
		public override function send (callback : Function) : void{
			super.send( 
				function(message:Object) : void{
					if(callback != null)
					{
						callback(new GetRunningTotalsResponse(message));
					}
				}
			);
		}
		



		/**
		* A friend id or an array of friend ids
		*/
		public function setFriendIds( friendIds : Vector.<String> ) : GetRunningTotalsRequest
		{
			this.data["friendIds"] = toArray(friendIds);
			return this;
		}



		/**
		* The short code of the running total
		*/
		public function setShortCode( shortCode : String ) : GetRunningTotalsRequest
		{
			this.data["shortCode"] = shortCode;
			return this;
		}
				
	}
	
}

