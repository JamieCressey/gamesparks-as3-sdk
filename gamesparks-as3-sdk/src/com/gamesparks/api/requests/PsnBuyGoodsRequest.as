
package com.gamesparks.api.requests
{

	import com.gamesparks.api.*;
	import com.gamesparks.api.responses.*;
	import com.gamesparks.*;
	
	
	/**
	* Processes an update of entitlement in PlayStation network.
	* The GameSparks platform will update the 'use_count' for an entitlement (by default 'use_count' is 1).
	* The request will be rejected if entitlement 'use_limit' is 0
	* GampSparks platform by default will use internally saved PSN user access token
	*/
	public class PsnBuyGoodsRequest extends GSRequest
	{
	
		public function setScriptData(scriptData:Object):PsnBuyGoodsRequest{
			data["scriptData"] = scriptData;
			return this;
		}
		
		function PsnBuyGoodsRequest(gs:GS)
		{
			super(gs);
			data["@class"] =  ".PsnBuyGoodsRequest";
		}
		
		/**
		* set the timeout for this request
		*/
		public function setTimeoutSeconds(timeoutSeconds:int=10):PsnBuyGoodsRequest
		{
			this.timeoutSeconds = timeoutSeconds; 
			return this;
		}
		
		/**
		* Send the request to the server. The callback function will be invoked with the response
		*/
		public override function send (callback : Function) : void{
			super.send( 
				function(message:Object) : void{
					if(callback != null)
					{
						callback(new BuyVirtualGoodResponse(message));
					}
				}
			);
		}
		



		/**
		* Specify the entitlement label of the entitlement to update. (Not an entitlement ID).
		*/
		public function setEntitlementLabel( entitlementLabel : String ) : PsnBuyGoodsRequest
		{
			this.data["entitlementLabel"] = entitlementLabel;
			return this;
		}


		/**
		* PlayStations network user access token.
		*/
		public function setPsnAccessToken( psnAccessToken : String ) : PsnBuyGoodsRequest
		{
			this.data["psnAccessToken"] = psnAccessToken;
			return this;
		}



		/**
		* If set to true, the transactionId from this receipt will not be globally valdidated, this will mean replays between players are possible.
		* It will only validate the transactionId has not been used by this player before.
		*/
		public function setUniqueTransactionByPlayer( uniqueTransactionByPlayer : Boolean ) : PsnBuyGoodsRequest
		{
			this.data["uniqueTransactionByPlayer"] = uniqueTransactionByPlayer;
			return this;
		}
				
	}
	
}

