
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!

package com.gamesparks.api.types
{

	import com.gamesparks.*;
	
	
	public class PlayerDetail extends GSData
	{
	
		public function PlayerDetail(data : Object)
		{
			super(data);
		}
	
	
		/// <summary>
		/// A player's external identifiers
		/// </summary>
		public function getExternalIds() : Object{
			if(data.externalIds != null)
			{
				return data.externalIds;
			}
			return null;
		}
		/// <summary>
		/// A player's id
		/// </summary>
		public function getId() : String{
			if(data.id != null)
			{
				return data.id;
			}
			return null;
		}
		/// <summary>
		/// A player's name
		/// </summary>
		public function getName() : String{
			if(data.name != null)
			{
				return data.name;
			}
			return null;
		}
	}

}

