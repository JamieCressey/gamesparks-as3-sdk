﻿package com.gamesparks
{
	import com.adobe.serialization.json.JSON;
	import com.gamesparks.api.messages.GSMessageHandler;
	import com.gamesparks.sockets.GSSocket;
	import com.gamesparks.sockets.GSSocketGimite;
	
	import flash.desktop.*;
	import flash.display.*;
	import flash.events.*;
	import flash.filters.*;
	import flash.text.*;
	import flash.utils.Dictionary;
	import flash.utils.clearTimeout;
	import flash.utils.setTimeout;
	
	public class GS
	{
		import flash.utils.Timer;
		import flash.events.TimerEvent;	
		import com.gamesparks.api.requests.*;
		
		//Internals
		private var _itemsToSend:Array = new Array();
		private var _pendingRequests:Dictionary = new Dictionary();
		
		private var queueTimeout:uint;
		
		private var _authToken:String;
		private var _sessionId:String;
		private var _initialised:Boolean = false;
		private var _initialising:Boolean = false;
		
		private var requestBuilder:GSRequestBuilder;
		private var messageHandler:GSMessageHandler = new GSMessageHandler();
		
		//Fluent builder stuff - START
		private var _messageCallback:Function;
		private var _availabilityCallback:Function;
		private var _apiSecret:String;
		private var _apiKey:String;
		private var _liveServers:Boolean = false;
		private var _url:String;
		private var _lbUrl:String;
		private var _socket:GSSocket;
		private var _logger:Function;
		
		private var stopped:Boolean = false;
		
		private var _stage:Stage;
		var _overlay:Sprite = new Sprite();
		private var _text:TextField = new TextField();
		private var _format:TextFormat = new TextFormat(); 
		
		public function GS(stage:Stage = null) 
		{
			_stage = stage;
			
			if (_stage != null)
			{
				_stage.addChild(_overlay);	
				
				_stage.addEventListener(Event.RESIZE, resizeListener); 
			}
			
			requestBuilder = new GSRequestBuilder(this);
		}
		
		private function resizeListener(e:Event):void
		{
			_text.y = _stage.stageHeight - _text.textHeight - 5;
			_text.width = _stage.stageWidth;
		}
		
		public function setAvailabilityCallback(value:Function):GS
		{
			_availabilityCallback = value;
			return this;
		}

		public function setApiSecret(value:String):GS
		{
			_apiSecret = value;
			return this;
		}
		
		public function setApiKey(value:String):GS
		{
			_apiKey = value;
			
			if(_liveServers){
				_url = "wss://service.gamesparks.net/ws/" +_apiKey;
				_lbUrl = _url;
			} else {
				_url = "wss://preview.gamesparks.net/ws/" +_apiKey
				_lbUrl = _url;
			}
			
			return this;
		}
		
		public function setUseLiveServices(value:Boolean):GS
		{
			_liveServers = value;
			
			if(_apiKey != null){
				setApiKey(_apiKey);
			}
			
			return this;
		}

		public function setUrl(value:String):GS
		{
			_url = value;
			_lbUrl = _url;
			return this;
		}

		public function setLogger(value:Function):GS
		{
			_logger = value;
			return this;
		}
		
		//Fluent builder stuff - END
		
		private function getUrl():String{
			if(_url == null){
				if(_apiKey == null){
					throw new Error("GameSparks missing either Url or apiKey");
				} else {
					setApiKey(_apiKey);
				}
			}
			return _url;
		}
		
		public function connect():void
		{
			if (_stage != null)
			{
				if (_text.stage)
				{
					_text.parent.removeChild(_text);
				}
				
				if (_liveServers == false)
				{	
					_format.color = 0xdddddd;
					_format.size = 18; 
					
					_text.text = "GameSparks Preview mode";
					_text.setTextFormat(_format);
					_text.y = _stage.stageHeight - _text.textHeight - 5;
					_text.width = _stage.stageWidth;
					
					_overlay.addChild(_text);
					_overlay.mouseEnabled = false;
					_overlay.mouseChildren = false;
				}
			}
			
			_initialising = true;
			stopped = false;
			_socket = new GSSocketGimite(log);
			_socket.Connect(_url, handleWebSocketOpen, handleWebSocketClosed, handleWebSocketMessage, handleWebSocketError);
		}

		public function send(request:GSRequest):void
		{
			//Need to do the sending here
			_itemsToSend.push(request);
			setTimeout(timeoutRequest, request.getTimeoutSeconds()*1000, request);
			processQueues();
		}
		
		private function timeoutRequest(request:GSRequest):void
		{
			var index:int = _itemsToSend.indexOf(request);
			
			var wasWaiting:Boolean = false;
			
			if(index != -1)
			{
				_itemsToSend.splice(index, 1);
				wasWaiting = true;
				
			}
			
			if(_pendingRequests[request.requestId] == request)
			{
				delete _pendingRequests[request.requestId];
				wasWaiting = true;
			}
			
			if(wasWaiting && request.callback != null)
			{
				var timeout:Object = new Object();
				timeout.error = new Object();
				timeout.error.error = "timeout";
				request.callback(timeout);
			}
			
		}
		
		
		public function disconnect():void
		{
			stopped = true;
			_socket.Disconnect();
		}
		
		public function getRequestBuilder():GSRequestBuilder
		{
			return requestBuilder;
		}
		
		public function getMessageHandler():GSMessageHandler
		{
			return messageHandler;
		}
		
		private function handleWebSocketClosed():void {
			
			log("Websocket closed. initialised="+_initialised+ " initialising="+_initialising);
			
			setAvailabile(false)
			
			if((_initialised || _initialising) && !stopped){
				connect();
			}
		}
		
		private function handleWebSocketError(error:String=""):void {
			log("Websocket Error " + error);
			_url = _lbUrl;
			setTimeout(handleWebSocketClosed, 1000);
		}
		
		private function handleWebSocketOpen():void {
			log("Websocket Connected");
		}
		
		private function processQueues(event:TimerEvent=null):void {
			if(queueTimeout)
				clearTimeout(queueTimeout);
			
			if(_socket.Connected())
			{
				if(_itemsToSend.length > 0)
				{
					var request:GSRequest = _itemsToSend.shift();
					try
					{
						var data:Object = request.getData();
						data.requestId = String(new Date().time) + String(Math.floor(Math.random() * (10000)));
						_socket.Send(com.adobe.serialization.json.JSON.encode(data));
						_pendingRequests[data.requestId] = request;
					} catch (e:Error) {
						_itemsToSend.unshift(_itemsToSend);
						return;
					}
				}
			} else {
				queueTimeout = setTimeout(processQueues, 1000);
			}
		}
		
		
		private function log(msg:String):void
		{
			if(_logger != null)
				_logger(msg);
		}
		
		private function saveSettings():void {
			import flash.net.SharedObject;
			var sharedObject:SharedObject = SharedObject.getLocal('settings');
			sharedObject.data.authToken = _authToken;
			sharedObject.flush();
		}
		
		private function handleWebSocketMessage(message:String):void {
			
			import com.adobe.utils.StringUtil;
			
			log("handleWebSocketMessage" + message);
			
			var response:Object = com.adobe.serialization.json.JSON.decode(message);
			
			if(response.authToken){
				_authToken = response.authToken;
				saveSettings();
				log("Got authtoken " + _authToken);
			}
			
			if(response.connectUrl != null){
				_url = response.connectUrl;
			}
			
			if(response.requestId && response.requestId != 0)
			{
				var request:GSRequest = _pendingRequests[response.requestId];
				delete _pendingRequests[response.requestId];
				if(request.callback != null){
					request.callback(response);
				}
			}
			else if(StringUtil.endsWith(response["@class"], "Message"))
			{
				messageHandler.handle(response);
			}
			else if(response["@class"] == ".AuthenticatedConnectResponse")
			{
				if(response.error) {
					log("INCORRECT APIKEY / APISECRET")
					disconnect();
				}
				if(response.sessionId != null){
					_sessionId = response.sessionId ;
				}
				
				if(response.nonce != null){
					//Need to send auth here.
					var toSend:Object = new Object();
					toSend["@class"]=".AuthenticatedConnectRequest";
					
					toSend.hmac = getHmac(response.nonce); 
					
					if(_authToken != "0"){
						toSend.authToken = _authToken;
					}
					if(_sessionId != null){
						toSend.sessionId = _sessionId;
					}
					
					var snd:String = com.adobe.serialization.json.JSON.encode(toSend);
					log("sending:"+snd);
					if(_socket.Connected()){
						_socket.Send(snd);
					}
					return;
				} else if ( response.connectUrl == null){
					_initialised = true;
					_initialising = false;
					setAvailabile(true);
					processQueues();
					
				}
			}
					
		}
		
		private function setAvailabile(availability:Boolean):void{
			if(_availabilityCallback != null){
				_availabilityCallback(availability);
			}
		}
		
		private function getHmac(nonce:String):String {
			
			import com.hurlant.crypto.hash.HMAC;
			import com.hurlant.crypto.hash.SHA256;
			import com.hurlant.util.Base64;
			import com.hurlant.util.Hex;
			import flash.utils.ByteArray;
			
			var hmac256:HMAC = new HMAC(new SHA256);
			var secretBytes:ByteArray = Hex.toArray( Hex.fromString(_apiSecret) ); 
			var authTokenBytes:ByteArray = Hex.toArray(Hex.fromString(nonce));
			return Base64.encodeByteArray(hmac256.compute(secretBytes, authTokenBytes));
		}
	}
}