package com.gamesparks.sockets
{
	public interface GSSocket
	{
		function Connect(url:String, onOpen:Function, onClose:Function, onMessage:Function, onError:Function):void;
			
		function Send(message:String):void;
		
		function Connected():Boolean;
		
		function Disconnect():void;
	}
}