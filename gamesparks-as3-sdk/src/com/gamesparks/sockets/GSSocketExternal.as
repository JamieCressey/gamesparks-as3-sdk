package com.gamesparks.sockets
{
	import flash.external.ExternalInterface;

	public class GSSocketExternal implements GSSocket
	{
		private var connected:Boolean = false;
		
		private var onOpen:Function, onClose:Function, onMessage:Function, onError:Function;
		
		public function Connect(url:String, onOpen:Function, onClose:Function, onMessage:Function, onError:Function):void
		{
			this.onError = onError;
			this.onOpen = onOpen;
			this.onMessage = onMessage;
			this.onClose = onClose;
			
			ExternalInterface.addCallback("OnClose", function():void { connected = false; onClose(); });
			ExternalInterface.addCallback("OnOpen", function():void { connected = true; onOpen(); });
			ExternalInterface.addCallback("OnMessage", function(msg:String):void { connected = false; onMessage(msg); });
			ExternalInterface.addCallback("OnError", function():void { connected = false; onError(); });
			
			ExternalInterface.call("GameSparksOpen", url);
			
		}
		
		public function Connected():Boolean
		{
			return connected;
		}
		
		public function Send(msg:String):void
		{
			ExternalInterface.call("GameSparksSend", msg);
		}
		
		public function Disconnect():void
		{
			ExternalInterface.call("GameSparksClose");
		}
	}
}