package com.gamesparks.sockets
{
	import flash.net.Socket;
	import flash.utils.ByteArray;
	import flash.utils.setTimeout;

	public class GSSocketGimite implements GSSocket
	{
		import net.gimite.websocket.*;
		
		private var websocket:WebSocket;
		private var secret:String;
		
		private var connected:Boolean = false;
		
		private var onOpen:Function, onClose:Function, onMessage:Function, onError:Function;
		
		private var logger:Function;
		
		public function GSSocketGimite(logger:Function)
		{
			this.logger = logger;
			
		}
		
		public function Connect(url:String, onOpen:Function, onClose:Function, onMessage:Function, onError:Function):void
		{
			this.onError = onError;
			this.onOpen = onOpen;
			this.onMessage = onMessage;
			this.onClose = onClose;
			
			websocket = new WebSocket(0, url, [], "*", null, 0, null, null, new GSSocketGimiteLogger(logger));
			
			websocket.addEventListener(WebSocketEvent.CLOSE, handleClose);
			websocket.addEventListener(WebSocketEvent.OPEN, handleOpen);
			websocket.addEventListener(WebSocketEvent.MESSAGE, handleMessage);
			websocket.addEventListener(WebSocketEvent.ERROR, handleError);
			websocket.addEventListener(WebSocketEvent.PONG, handlePong);
		}
		
		private function handleClose(event:WebSocketEvent):void
		{
			connected = false; 
			if(event.code == 1006){
				onError("1006");
			} else {
				onClose();	
			}
		}
		
		private function handleOpen(event:WebSocketEvent):void
		{
			connected = true; 
			onOpen(); 
			keepAlive()
		}
		
		private function handleMessage(event:WebSocketEvent):void
		{
			onMessage(event.message);
		}
		
		private function handleError():void
		{
			onError();
		}
		
		public function Connected():Boolean
		{
			return websocket.getReadyState() == 1;
		}
		
		public function Send(msg:String):void
		{
			websocket.send(msg);
		}
		
		public function Disconnect():void
		{
			websocket.close();
		}
		
		private var waitingForPong:Boolean = false;
		
		public function keepAlive():void{
			if(!connected) {
				return;
			}
			websocket.sendPing(new ByteArray());
			waitingForPong = true;
			setTimeout(function():void {
				if(waitingForPong){
					websocket.close(1006);
				}
				keepAlive();
			}, 10000);
		}
		
		private function handlePong(event:WebSocketEvent):void
		{
			waitingForPong = false;
		}
	}
}