package com.gamesparks
{
	
	public class GSRequest extends GSData
	{
		
		protected var timeoutSeconds:int = 5;
		internal var callback:Function;
		internal var requestId:String;
		
		private var gs:GS;
		
		public function GSRequest(gs:GS)
		{
			super(new Object());
			this.gs = gs;
		}
		
		public function getData() : Object
		{
			return data;
		}
		
		public function send (callback : Function) : void{
			this.callback = callback;
			gs.send(this);
		}
		
		internal function getTimeoutSeconds():int
		{
			return timeoutSeconds;
		}
		
		protected function toArray(iterable:*):Array {
			var ret:Array = [];
			for each (var elem:* in iterable) ret.push(elem);
			return ret;
		}
	}
	
}